##Authenticatin Related Functions
import requests

import general


def login(username,password): ##Authenticate with AD and return login cookie
    r = general.post("http://animediscuss.ml/members/login.php",{"username":username,"password":password,"memorize":"yes"})

    if r.text.__contains__('You have successfully been logged'):
        cookies = r.headers["Set-Cookie"]

        ##Remove Duplicate Cookies
        while cookies.__contains__(" expires="):
            a = cookies.index(' expires=')
            cookies2 = cookies[a:]
            a = cookies2.index(";")
            cookies2 = cookies2[:a+1]
            cookies = cookies.replace(cookies2,"")
        while cookies.__contains__(" Max-Age="):
            a = cookies.index(' Max-Age=')
            cookies2 = cookies[a:]
            a = cookies2.index(";")
            cookies2 = cookies2[:a+1]
            cookies = cookies.replace(cookies2, "")


        cookies = cookies.replace("path=/; ","")
        cookies = cookies.replace("path=/, ", "")
        cookies = cookies.replace("domain=.animediscuss.ml;","")
        return cookies

    elif r.text.__contains__("Ups. Too many attempts! Try later."):
        return "attemptLimitReached"




