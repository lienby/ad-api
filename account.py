##Account-Related Functions
import requests

import general


### NO AUTHENTICAITON REQUIRED
def lookupAccountData(id):
    rawData = general.getHTML("http://animediscuss.ml/members/"+str(id))

    ##Determine if user is online
    if rawData.__contains__("<img src=/css/online.png></h2>"):
        isOnline = True
    else:
        isOnline = False

    ##Determine if user element
    if rawData.__contains__("/css/Water.gif"):
        element = "water"
    elif rawData.__contains__("/css/Fire.gif"):
        element = "fire"
    elif rawData.__contains__("/css/Earth.gif"):
        element = "earth"
    elif rawData.__contains__("/css/Air.gif"):
        element = "Air"
    else:
        element = "Unknown"

    ##Determine user gender
    if rawData.__contains__("Male"):
        gender = "male"
    elif rawData.__contains__("Female"):
        gender = "female"
    else:
        gender = "unknown"

    ##Determine user rank
    if rawData.__contains__('<h2 style="font-size:27px"><u>'):
        a = rawData.index('<h2 style="font-size:27px"><u>')+len('<h2 style="font-size:27px"><u>')
        rank = rawData[a:]
        a = rank.index('</u>')
        rank = rank[:a]
        rank = str(rank)
    else:
        rank = "Unknown"

    ##Determine username
    if rawData.__contains__('style="width:200px;height:200px;border-radius:50px;border:1px solid black"><br>\n<h2>'):
        a = rawData.index('style="width:200px;height:200px;border-radius:50px;border:1px solid black"><br>\n<h2>')+len('style="width:200px;height:200px;border-radius:50px;border:1px solid black"><br>\n<h2>')
        username = rawData[a:]
        a = username.index('<')
        username = str(username[:a])
    else:
        username = "Unknown"

    ##Is user claimed?
    if rawData.__contains__("Claimed by:"):
        claimed = True
    else:
        claimed = False

    ##If claimed. who by and how much and when?
    if claimed == True:
        a = rawData.index("Claimed by:  <td>")+len("Claimed by:  <td>")
        claimedBy = rawData[a:]
        a = claimedBy.index(" for ")
        claimAmount = claimedBy[a+len(" for "):]
        claimedBy = claimedBy[:a]
        a = claimAmount.index(" coins on ")
        claimDate = claimAmount[a+len(" coins on "):]
        claimAmount = int(claimAmount[:a])
        a = claimDate.index("</tr><tr><td>")
        claimDate = claimDate[:a]
    else:
        claimedBy = "Unknown"
        claimAmount = "Unknown"
        claimDate = "Unknown"

    ##Get coin count
    if rawData.__contains__("Coins:  <td>"):
        a = rawData.index("Coins:  <td>")+len("Coins:  <td>")
        coins = rawData[a:]
        a = coins.index("</tr><tr><td>")
        coins = int(coins[:a])
    else:
        coins = 0


    ##Check for "About Me" and obtain it if exists
    if rawData.__contains__("About:<br><td>"):
        a = rawData.index("About:<br><td>")+len("About:<br><td>")
        about = rawData[a:]
        a = about.index("</table>")
        about = about[:a]
    else:
        about = "Unknown"

    ##Check for "Location" and obtain if it exists
    if rawData.__contains__("Location: <td>"):
        a = rawData.index("Location: <td>")+len("Location: <td>")
        location = rawData[a:]
        a = location.index("</tr><tr><td>")
        location = location[:a]
    else:
        location = "Unknown"

    ##Get Birthday And Age
    if rawData.__contains__("Age:  <td>"):
        a = rawData.index("Age:  <td>")+len("Age:  <td>")
        birthday = rawData[a:]
        a = birthday.index(" (")
        age = birthday[a+len(" ("):]
        birthday = str(birthday[:a])
        a = age.index(" Years old.)</tr><tr><td>")
        age = int(age[:a])
    else:
        age = 0
        birthday = "unknown"

    ##Get Reputation & ReputationLevel
    if rawData.__contains__("Reputation:  <td>"):
        a = rawData.index("Reputation:  <td>")+len("Reputation:  <td>")
        reputation = rawData[a:]
        a = reputation.index(" (")
        reputationLevel = reputation[a+len(" ("):]
        reputation = int(reputation[:a])
        a = reputationLevel.index(")</tr><tr><td>")
        reputationLevel = reputationLevel[:a]
    else:
        reputation = 0
        reputationLevel = "Unknown"


    ##Get joinDate
    if rawData.__contains__("Date Joined: <td> "):
        a = rawData.index("Date Joined: <td> ")+len("Date Joined: <td> ")
        joinDate = rawData[a:]
        a = joinDate.index("<tr><td>")
        joinDate = joinDate[:a]
    else:
        joinDate = "unknown"

    ##Get lastLoginDate
    if rawData.__contains__("Last Login: <td> "):
        a = rawData.index("Last Login: <td> ")+len("Last Login: <td> ")
        lastLoginDate = rawData[a:]
        a = lastLoginDate.index("</tr><tr><td>")
        lastLoginDate = lastLoginDate[:a]
    else:
        lastLoginDate = "unknown"

    ##Get profilePicUrl
    if rawData.__contains__('<aside class="sidebar big-sidebar left-sidebar">\n<center><img src="'):
        a = rawData.index('<aside class="sidebar big-sidebar left-sidebar">\n<center><img src="')+len('<aside class="sidebar big-sidebar left-sidebar">\n<center><img src="')
        profilePicUrl = rawData[a:]
        a = profilePicUrl.index('" style="width:200px;height:200px;border-radius:50px;border:1px solid black"><br>')
        profilePicUrl = profilePicUrl[:a]
    else:
        profilePicUrl = "unknown"

    ##Get commentCount
    if rawData.__contains__("<h2 style='font-size:25px'>Comments ("):
        a = rawData.index("<h2 style='font-size:25px'>Comments (")+len("<h2 style='font-size:25px'>Comments (")
        commentCount = rawData[a:]
        a = commentCount.index(")</h2>")
        commentCount = int(commentCount[:a])
    else:
        commentCount = int(0)

    if not rawData.__contains__("This user does not exist."):
        return {'isOnline':isOnline,'element':element,"gender":gender,"rank":rank,"username":username,"claimed":claimed,"claimedBy":claimedBy,'claimAmount':claimAmount,'claimDate':claimDate,'coins':coins,'about':about,'location':location,'birthday':birthday,'age':age,'reputation':reputation,'reputationLevel':reputationLevel,'joinDate':joinDate,'lastLoginDate':lastLoginDate,"profilePicUrl":profilePicUrl,'commentCount':commentCount}
    else:
        return "Unknown"

def getAccountId(name):
    rawData = general.getHTML("http://animediscuss.ml/members/search.php?q="+str(name))
    ##Does this user acturally exist?
    if rawData.__contains__("><h2>"+str(name)+"</a></td>"):
        a = rawData.index('/"><h2>'+str(name)+"</a></td>")
        id = rawData[:a]
        a = id.index('<td style="background:transparent;border:0"><a href="/members/')+len('<td style="background:transparent;border:0"><a href="/members/')
        id = int(id[a:])
        return id
    else:
        return("Unknown")

def isOnline(id): ## Checks if account is onliine
    return lookupAccountData(id)['isOnline']

def getElement(id): ## Gets 'element' (fire water earth, air)
    return lookupAccountData(id)['element']

def getGender(id): ## Gets user gender (male/female)
    return lookupAccountData(id)['gender']

def getRank(id): ## Gets user rank (admin,member)
    return lookupAccountData(id)['rank']

def getUsername(id): ## Gets Username from an AccountID
    return lookupAccountData(id)['username']

def isClaimed(id): ## Check if a user is claimed
    return lookupAccountData(id)['claimed']

def getClaimOwner(id): ## Get Claim Owner
    return lookupAccountData(id)['claimedBy']

def getClaimPrice(id): ## Get Claim Price
    return lookupAccountData(id)['claimAmount']

def getClaimDate(id): ## Get Claim Date
    return lookupAccountData(id)['claimDate']

def getCoins(id): ## Get Users Coin Count
    return lookupAccountData(id)['coins']

def getAbout(id): ##Get The "About Me" Page
    return lookupAccountData(id)['about']

def getLocation(id): ## Get Location
    return lookupAccountData(id)['location']

def getBirthdate(id): ## Get Users Birthday
    return lookupAccountData(id)['birthday']

def getAge(id): ## Get Users Age
    return lookupAccountData(id)['age']

def getReputation(id): ## Get User Reputation
    return lookupAccountData(id)['reputation']

def getReputationLevel(id): ## Get User Reputation Level (Active/God)
    return lookupAccountData(id)['reputationLevel']

def getCreationDate(id): ## Get Users First-join date
    return lookupAccountData(id)['joinDate']

def getLastLogin(id): ## Get Users LastLogin Date
    return lookupAccountData(id)['lastLoginDate']

def getAvatar(id): ## Gets User Avatar, or Profile Picture URL
    return lookupAccountData(id)['profilePicUrl']

def getCommentCount(id): ## Get Number Of Comments On Users Page
    return lookupAccountData(id)['commentCount']


##REQUIRES AUTHENTICATION

def getNotificationCount(authcookie): ## GET NUMBER OF NOTIFICATIONS
    rawData = general.getHTML("http://animediscuss.ml",authcookie)
    a = rawData.index('<li><a href="/members/notifications.php">Notifications (')+len('<li><a href="/members/notifications.php">Notifications (')
    notificationCount = rawData[a:]
    a = notificationCount.index(')</a></li>')
    notificationCount = int(notificationCount[:a])
    return notificationCount

def getMessageCount(authcookie): ## GET NUMBER OF MESSAGES
    rawData = general.getHTML("http://animediscuss.ml",authcookie)
    a = rawData.index('Inbox (')+len('Inbox (')
    messageCount = rawData[a:]
    a = messageCount.index(')</a></li>')
    messageCount = int(messageCount[:a])
    return messageCount

def addFriend(authcookie,id): ##Send friend Request to user
    headers = {'Cookie': authcookie,'User-Agent': 'AD-API'}
    r = requests.get("http://animediscuss.ml/members/friends.php?id="+str(id)+"&action=add", headers=headers)
    if r.text.__contains__("Sent A Request!"):
        return True
    else:
        return False
def removeFriend(authcookie,id): ##Unfriend user
    headers = {'Cookie': authcookie,'User-Agent': 'AD-API'}
    r = requests.get("http://animediscuss.ml/members/friends.php?id="+str(id)+"&action=remove", headers=headers)
    if r.text.__contains__("Unfriended!"):
        return True
    else:
        return False