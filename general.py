##General Functions
import requests
from Crypto.Cipher import AES




def getCookie(): ##Bypasses AD's BOT Detection

    data = requests.get("http://animediscuss.ml/",headers={'User-Agent': 'AD-API'}).text
    breakPoint = data.index('a=toNumbers("')+len('a=toNumbers("')
    key = data[breakPoint:]
    breakPoint = key.index('"),')
    key = key[:breakPoint]
    breakPoint = data.index('b=toNumbers("')+len('b=toNumbers("')
    iv = data[breakPoint:]
    breakPoint = iv.index('"),')
    iv = iv[:breakPoint]
    breakPoint = data.index('c=toNumbers("')+len('c=toNumbers("')
    ciphertext = data[breakPoint:]
    breakPoint = ciphertext.index('");')
    ciphertext = ciphertext[:breakPoint]
    decrypter = AES.new(key.decode('hex'), 2,iv.decode('hex'))
    PlainText = decrypter.decrypt(ciphertext.decode('hex'))
    return PlainText.encode('hex')



#Cookie = getCookie()
Cookie = ""


def getHTML(url,cookie=""): ##Download HTML Data while bypassing TestCookie
    #headers = {'Cookie': '__test='+Cookie+';'+cookie,'User-Agent': 'AD-API'}
    headers = {'Cookie': cookie , 'User-Agent': 'AD-API'}
    url = url
    return requests.get(url, headers=headers).text

def post(url,postdata,cookie=""): ##Send POST Data while bypassing TestCookie
    headers = {'Cookie': cookie , 'User-Agent': 'AD-API'}
    return requests.post(url,data=postdata,headers=headers)

